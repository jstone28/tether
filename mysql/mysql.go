package mysql

import (
	"database/sql"
	"log"

	// mysql driver to use in conjunction with database/sql
	_ "github.com/go-sql-driver/mysql"
)

// Connect connects to the database
func Connect() {
	db, err := sql.Open("mysql", "mysql:mysql@tcp(localhost:3306)/tether")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
	rows, err := db.Query("CREATE TABLE IF NOT EXISTS tether(username VARCHAR(30));")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
}
