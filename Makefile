run:
	go run tether.go

build:
	go build tether.go

deploy:
	docker build -t registry.gitlab.com/jstone28/tether .
	docker push registry.gitlab.com/jstone28/tether
