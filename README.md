# tether

A MySQL/PostgreSQL Comparator

## Overview

Tether uses one `master` and one `slave` database, and ensures data integrity in the `slave` as the `master` as the source of truth. This connection will occur as a perpetual process and continually reconcile the `slave`'s data against the `master`'s as its updated.

![tether](images/tether.png)

## Getting Started

Kick off the reconciliation process

```bash
make run
```
